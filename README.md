# Calculatrice Android

This is a simple calculator for Android, which aims to mimic the behaviour of a HP28S calculator.
It is my first app using Kotlin, so it is also a bit of a testbed.

## Features

+ Only the RPN mode is implemented
+ The operationnal stack is here, and we can drop the first element
+ The four main operations are implemented (+, -, *, /)
+ Square, square root, inverse, power are implemented
+ Some mathematical functions are here:
  - sin, cos, tan
  - asin, acos, atan

## Compilation

It is an Android Studio project, so just open it with AS, and click on compile.
