package net.dindinx.calculatrice

//import android.icu.lang.UCharacter.GraphemeClusterBreak.*
//import android.os.Build.VERSION_CODES.*
import android.os.Bundle
import android.view.View
import android.widget.Button
//import android.widget.Space
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*

class MainActivity : AppCompatActivity()
{
    private lateinit var tvResult:  TextView
    private lateinit var tvStack1:  TextView
    private lateinit var tvStack2:  TextView
    private lateinit var tvStack3:  TextView
    private lateinit var tvStack4:  TextView
    private lateinit var operationStack: Vector<Double>
    private lateinit var btnOne:    Button
    private lateinit var btnTwo:    Button
    private lateinit var btnThree:  Button
    private lateinit var btnFour:   Button
    private lateinit var btnFive:   Button
    private lateinit var btnSix:    Button
    private lateinit var btnSeven:  Button
    private lateinit var btnEight:  Button
    private lateinit var btnNine:   Button
    private lateinit var btnZero:   Button
    private lateinit var btnDot:    Button
    private lateinit var btnComma:  Button
    private lateinit var btnEnter:  Button
    private lateinit var btnPlus:   Button
    private lateinit var btnMinus:  Button
    private lateinit var btnTimes:  Button
    private lateinit var btnDivide: Button
    private lateinit var btnCursor: Button
    private lateinit var btnTrig:   Button
    private lateinit var btnSolv:   Button
    private lateinit var btnUser:   Button
    private lateinit var btnNext:   Button
    private lateinit var btnChs:    Button
    private lateinit var btnEex:    Button
    private lateinit var btnDrop:   Button
    private lateinit var btnBack:   Button
    private lateinit var btnQuote:  Button
    private lateinit var btnSto:    Button
    private lateinit var btnEval:   Button
    private lateinit var btnOn:     Button

    // Left keyboard
    private lateinit var btnA:                  Button
    private lateinit var btnB:                  Button
    private lateinit var btnC:                  Button
    private lateinit var btnD:                  Button
    private lateinit var btnE:                  Button
    private lateinit var btnF:                  Button
    private lateinit var btnG:                  Button
    private lateinit var btnH:                  Button
    private lateinit var btnI:                  Button
    private lateinit var btnJ:                  Button
    private lateinit var btnK:                  Button
    private lateinit var btnL:                  Button
    private lateinit var btnM:                  Button
    private lateinit var btnN:                  Button
    private lateinit var btnO:                  Button
    private lateinit var btnP:                  Button
    private lateinit var btnQ:                  Button
    private lateinit var btnR:                  Button
    private lateinit var btnS:                  Button
    private lateinit var btnT:                  Button
    private lateinit var btnU:                  Button
    private lateinit var btnV:                  Button
    private lateinit var btnW:                  Button
    private lateinit var btnX:                  Button
    private lateinit var btnY:                  Button
    private lateinit var btnZ:                  Button
    private lateinit var btnHash:               Button
    private lateinit var btnCurlyBracket:       Button
    private lateinit var btnSquareBracket:      Button
    private lateinit var btnParenthesis:        Button
    private lateinit var btnSpace:              Button
    private lateinit var btnDoubleAngleBracket: Button
    private lateinit var btnEqual:              Button
    private lateinit var btnLC:                 Button
    private lateinit var btnAlpha:              Button

    // Menus buttons
    private lateinit var btnInsert: Button
    private lateinit var btnDelete: Button
    private lateinit var btnUp:     Button
    private lateinit var btnDown:   Button
    private lateinit var btnLeft:   Button
    private lateinit var btnRight:  Button
    enum class MENU
    {
        CURSOR,
        ARRAY1, ARRAY2, ARRAY3, ARRAY4,
        BINARY1, BINARY2, BINARY3, BINARY4,
        COMPLEX1, COMPLEX2,
        STRING1, STRING2,
        LIST1, LIST2,
        REAL1, REAL2, REAL3, REAL4,
        STACK1, STACK2,
        STORE1, STORE2,
        ALGEBRA1, ALGEBRA2,
        PRINT1, PRINT2,
        CONTROL1, CONTROL2,
        BRANCH1, BRANCH2, BRANCH3,
        TEST1, TEST2, TEST3,
        CATALOG,
        UNITS,
        MODE1, MODE2, MODE3,
        TRIG1, TRIG2, TRIG3,
        LOGS1, LOGS2,
        SOLV1, SOLV2,
        STAT1, STAT2, STAT3, STAT4,
        USER,
        PLOT1, PLOT2, PLOT3, PLOT4
    }
    private var current_menu: MENU = MENU.CURSOR
    private var menu_labels = arrayOf(
        arrayOf("INS",   "DEL",   "UP",    "DOWN",  "LEFT",  "RIGHT"), // Cursor
        arrayOf(">ARRY", "ARRY>", "PUT",   "GET",   "PUTI",  "GETI"),  // Array1
        arrayOf("SIZE",  "RDM",   "TRN",   "CON",   "IDN",   "RSD"),   // Array2
        arrayOf("CROSS", "DOT",   "DET",   "ABS",   "RNRM",  "CNRM"),  // Array3
        arrayOf("R>C",   "C>R",   "RE",    "IM",    "CONJ",  "NEG"),   // Array4
        arrayOf("DEC",   "HEX",   "OCT",   "BIN",   "STWS",  "RCWS"),  // Binary1
        arrayOf("RL",    "RR",    "RLB",   "RRB",   "R>B",   "B>R"),   // Binary2
        arrayOf("SL",    "SR",    "SLB",   "SRB",   "ASR",   " "),     // Binary3
        arrayOf("AND",   "OR",    "XOR",   "NOT",   " ",     " "),     // Binary4
        arrayOf("R>C",   "C>R",   "RE",    "IM",    "CONJ",  "SIGN"),  // Complex1
        arrayOf("R>P",   "P>R",   "ABS",   "NEG",   "ARG",   " "),     // Complex2
        arrayOf(">STR",  "STR>",  "CHR",   "NUM",   "POS",   "DISP"),  // String1
        arrayOf("SUB",   "SIZE",  " ",     " ",     " ",     " "),     // String2
        arrayOf(">LIST", "LIST>", "PUT",   "GET",   "PUTI",  "GETI"),  // List1
        arrayOf("SUB",   "SIZE",  " ",     " ",     " ",     " "),     // List2
        arrayOf("NEG",   "FACT",  "RAND",  "RDZ",   "MAXR",  "MINR"),  // Real1
        arrayOf("ABS",   "SIGN",  "MANT",  "XPON",  " ",     " "),     // Real2
        arrayOf("IP",    "FP",    "FLOOR", "CEIL",  "RND",   " "),     // Real3
        arrayOf("MAX",   "MIN",   "MOD",   "%T",    " ",     " "),     // Real4
        arrayOf("DUP",   "OVER",  "DUP2",  "DROP2", "ROT",   "LIST>"), // Stack1
        arrayOf("ROLLD", "PICK",  "DUPN",  "DROPN", "DEPTH", ">LIST"), // Stack2
        arrayOf("STO+",  "STO-",  "STO*",  "STO/",  "SNEG",  "SINV"),  // Store1
        arrayOf("SCONJ", " ",     " ",     " ",     " ",     " "),     // Store2
        arrayOf("COLCT", "EXPAN", "SIZE",  "FORM",  "OBSUB", "EXSUB"), // Algebra1
        arrayOf("TAYLR", "ISOL",  "QUAD",  "SHOW",  "OBGET", "EXGET"), // Algebra2
        arrayOf("PR1",   "PRST",  "PRVAR", "PRLCD", "TRACE", "NORM"),  // Print1
        arrayOf("PRSTC", "PRUSR", "PRMD",  "CR",    " ",     " "),     // Print2
        arrayOf("SST",   "HALT",  "ABORT", "KILL",  "WAIT",  "KEY"),   // Control1
        arrayOf("BEEP",  "CLLCD", "DISP",  "CLMF",  "ERRN",  "ERRM"),  // Control2
        arrayOf("IF",    "IFERR", "THEN",  "ELSE",  "END",   " "),     // Branch1
        arrayOf("START", "FOR",   "NEXT",  "STEP",  "IFT",   "IFTE"),  // Branch2
        arrayOf("DO",    "UNTI",  "END",   "WHIL",  "REPEA", "END"),   // Branch3
        arrayOf("SF",    "CF",    "FS?",   "FC?",   "FS?C",  "FC?C"),  // Test1
        arrayOf("AND",   "OR",    "XOR",   "NOT",   "SAME",  "=="),    // Test2
        arrayOf("STOF",  "RCLF",  "TYPE",  " ",     " ",     " "),     // Test3
        arrayOf("NEXT",  "PREV",  "SCAN",  "USE",   "FETCH", "QUIT"),  // Catalog
        arrayOf("NEXT",  "PREV",  "SCAN",  "USE",   "FETCH", "QUIT"),  // Units
        arrayOf("STD",   "FIX",   "SCI",   "ENG",   "DEG",   "RAD"),   // Mode1
        arrayOf("+CMD",  "-CMD",  "+LAST", "-LAST", "+UND",  "-UND"),  // Mode2
        arrayOf("+ML",   "-ML",   "RDX.",  "RDX,",  "PRMD",  " "),     // Mode3
        arrayOf("SIN",   "ASIN",  "COS",   "ACOS",  "TAN",   "ATAN"),  // Trig1
        arrayOf("P>R",   "R>P",   "R>C",   "C>R",   "ARG",   " "),     // Trig2
        arrayOf(">HMS",  "HMS>",  "HMS+",  "HMS-",  "D>R",   "R>D"),   // Trig3
        arrayOf("LOG",   "ALOG",  "LN",    "EXP",   "LNP1",  "EXPM"),  // Logs1
        arrayOf("SINH",  "ASINH", "COSH",  "ACOSH", "TANH",  "ATANH"), // Logs2
        arrayOf("STEQ",  "RCEQ",  "SOLVR", "ISOL",  "QUAD",  "SHOW"),  // Solv1
        arrayOf("ROOT",  " ",     " ",     " ",     " ",     " "),     // Solv2
        arrayOf("S+",    "S-",    "NS",    "CLS",   "STOS",  "RCLS"),  // Stat1
        arrayOf("TOT",   "MEAN",  "SDEV",  "VAR",   "MAXS",  "MINS"),  // Stat2
        arrayOf("COLS",  "CORR",  "COV",   "LR",    "PREDV", " "),     // Stat3
        arrayOf("UTPC",  "UTPF",  "UTPN",  "UTPT",  " ",     " "),     // Stat4
        arrayOf(" ",     " ",     " ",     " ",     " ",     " "),     // User
        arrayOf("STEQ",  "RCEQ",  "PMIN",  "PMAX",  "INDEP", "DRAW"),  // Plot1
        arrayOf("PPAR",  "RES",   "AXES",  "CENTR", "*W",    "*H"),    // Plot2
        arrayOf("STOS",  "RCLS",  "COLS",  "SCLS",  "DRWS",  " "),     // Plot3
        arrayOf("CLLCD", "DISP",  "PIXEL", "DRAX",  "CLMF",  "PRLCD")  // Plot4
    )
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        operationStack = Vector<Double>()
        tvResult = findViewById<TextView>(R.id.tvResult)
        tvStack1  = findViewById<TextView>(R.id.tvStack1)
        tvStack2  = findViewById<TextView>(R.id.tvStack2)
        tvStack3  = findViewById<TextView>(R.id.tvStack3)
        tvStack4  = findViewById<TextView>(R.id.tvStack4)
        /* Number Buttons */
        btnOne   = findViewById<Button>(R.id.bOne)
        btnTwo   = findViewById<Button>(R.id.bTwo)
        btnThree = findViewById<Button>(R.id.bThree)
        btnFour  = findViewById<Button>(R.id.bFour)
        btnFive  = findViewById<Button>(R.id.bFive)
        btnSix   = findViewById<Button>(R.id.bSix)
        btnSeven = findViewById<Button>(R.id.bSeven)
        btnEight = findViewById<Button>(R.id.bEight)
        btnNine  = findViewById<Button>(R.id.bNine)
        btnZero  = findViewById<Button>(R.id.bZero)
        btnDot   = findViewById<Button>(R.id.bDot)
        btnComma = findViewById<Button>(R.id.bComma)
        btnOne.setOnClickListener(this::onDigit)
        btnTwo.setOnClickListener(this::onDigit)
        btnThree.setOnClickListener(this::onDigit)
        btnFour.setOnClickListener(this::onDigit)
        btnFive.setOnClickListener(this::onDigit)
        btnSix.setOnClickListener(this::onDigit)
        btnSeven.setOnClickListener(this::onDigit)
        btnEight.setOnClickListener(this::onDigit)
        btnNine.setOnClickListener(this::onDigit)
        btnZero.setOnClickListener(this::onDigit)
        btnDot.setOnClickListener(this::onDot)
        btnComma.setOnClickListener(this::onComma)

        btnEnter = findViewById<Button>(R.id.bEnter)
        btnEnter.setOnClickListener(this::onEnter)
        // Base Operators
        btnPlus = findViewById<Button>(R.id.bPlus)
        btnPlus.setOnClickListener(this::onPlus)
        btnMinus = findViewById<Button>(R.id.bMinus)
        btnMinus.setOnClickListener(this::onMinus)
        btnTimes = findViewById<Button>(R.id.bTimes)
        btnTimes.setOnClickListener(this::onTimes)
        btnDivide = findViewById<Button>(R.id.bDivide)
        btnDivide.setOnClickListener(this::onDivide)

        val btnRed: Button = findViewById<Button>(R.id.bRed)
        btnRed.setOnClickListener(this::onRed)

        btnCursor = findViewById<Button>(R.id.bCursor)
        btnTrig   = findViewById<Button>(R.id.bTrig)
        btnTrig.setOnClickListener(this::onTrig)
        btnSolv   = findViewById<Button>(R.id.bSolv)
        btnUser   = findViewById<Button>(R.id.bUser)
        btnNext   = findViewById<Button>(R.id.bNext)
        btnChs    = findViewById<Button>(R.id.bChs)
        btnChs.setOnClickListener(this::onChs)
        btnEex    = findViewById<Button>(R.id.bEex)
        btnEex.setOnClickListener(this::onEex)
        btnDrop   = findViewById<Button>(R.id.bDrop)
        btnDrop.setOnClickListener(this::onDrop)
        btnBack   = findViewById<Button>(R.id.bBack)
        btnQuote  = findViewById<Button>(R.id.bQuote)
        btnSto    = findViewById<Button>(R.id.bSto)
        btnEval   = findViewById<Button>(R.id.bEval)
        btnOn     = findViewById<Button>(R.id.bOn)

        btnInsert = findViewById<Button>(R.id.bInsert)
        btnInsert.setOnClickListener(this::onInsert)
        btnDelete = findViewById<Button>(R.id.bDelete)
        btnDelete.setOnClickListener(this::onDelete)
        btnUp     = findViewById<Button>(R.id.bUp)
        btnUp.setOnClickListener(this::onUp)
        btnDown   = findViewById<Button>(R.id.bDown)
        btnDown.setOnClickListener(this::onDown)
        btnLeft   = findViewById<Button>(R.id.bLeft)
        btnLeft.setOnClickListener(this::onLeft)
        btnRight  = findViewById<Button>(R.id.bRight)
        btnRight.setOnClickListener(this::onRight)

        // Left keyboard
        btnA                  = findViewById<Button>(R.id.bA)
        btnB                  = findViewById<Button>(R.id.bB)
        btnC                  = findViewById<Button>(R.id.bC)
        btnD                  = findViewById<Button>(R.id.bD)
        btnE                  = findViewById<Button>(R.id.bE)
        btnF                  = findViewById<Button>(R.id.bF)
        btnG                  = findViewById<Button>(R.id.bG)
        btnH                  = findViewById<Button>(R.id.bH)
        btnI                  = findViewById<Button>(R.id.bI)
        btnJ                  = findViewById<Button>(R.id.bJ)
        btnK                  = findViewById<Button>(R.id.bK)
        btnL                  = findViewById<Button>(R.id.bL)
        btnM                  = findViewById<Button>(R.id.bM)
        btnN                  = findViewById<Button>(R.id.bN)
        btnO                  = findViewById<Button>(R.id.bO)
        btnP                  = findViewById<Button>(R.id.bP)
        btnQ                  = findViewById<Button>(R.id.bQ)
        btnR                  = findViewById<Button>(R.id.bR)
        btnS                  = findViewById<Button>(R.id.bS)
        btnT                  = findViewById<Button>(R.id.bT)
        btnU                  = findViewById<Button>(R.id.bU)
        btnV                  = findViewById<Button>(R.id.bV)
        btnW                  = findViewById<Button>(R.id.bW)
        btnX                  = findViewById<Button>(R.id.bX)
        btnY                  = findViewById<Button>(R.id.bY)
        btnZ                  = findViewById<Button>(R.id.bZ)
        btnHash               = findViewById<Button>(R.id.bHash)
        btnCurlyBracket       = findViewById<Button>(R.id.bCurlyBracket)
        btnSquareBracket      = findViewById<Button>(R.id.bSquareBracket)
        btnParenthesis        = findViewById<Button>(R.id.bParenthesis)
        btnSpace              = findViewById<Button>(R.id.bSpace)
        btnDoubleAngleBracket = findViewById<Button>(R.id.bDoubleAngleBracket)
        btnEqual              = findViewById<Button>(R.id.bEqual)
        btnLC                 = findViewById<Button>(R.id.bLC)
        btnAlpha              = findViewById<Button>(R.id.bAlpha)

        updateStackView()
    }
    var dot:Boolean = false
    var exp:Boolean = false
    var num:Boolean = false
    var red:Boolean = false
    var have_expression = false

    private fun updateStackView()
    {
        if (operationStack.size > 0)
            tvStack1.text = operationStack[operationStack.size - 1].toString()
        else
            tvStack1.text = getString(R.string.labelEmpty)
        if (operationStack.size > 1)
            tvStack2.text = operationStack[operationStack.size - 2].toString()
        else
            tvStack2.text = getString(R.string.labelEmpty)
        if (operationStack.size > 2)
            tvStack3.text = operationStack[operationStack.size - 3].toString()
        else
            tvStack3.text = getString(R.string.labelEmpty)
        if (operationStack.size > 3)
            tvStack4.text = operationStack[operationStack.size - 4].toString()
        else
            tvStack4.text = getString(R.string.labelEmpty)
    }
    private fun onRed(@Suppress("UNUSED_PARAMETER") view: View)
    {
        if (!red)
            updateButtonsToRed()
        else
            updateButtonsToUnred()
    }
    private fun updateButtonsToRed()
    {
        btnOne.text    = getString(R.string.labelCont)
        btnTwo.text    = getString(R.string.labelPercent)
        btnThree.text  = getString(R.string.labelPercentCH)
        btnFour.text   = getString(R.string.labelPurge)
        btnFive.text   = getString(R.string.labelIntegral)
        btnSix.text    = getString(R.string.labelDerivated)
        btnSeven.text  = getString(R.string.labelCommand)
        btnEight.text  = getString(R.string.labelUndo)
        btnNine.text   = getString(R.string.labelLast)
        btnZero.text   = getString(R.string.labelClear)
        btnPlus.text = getString(R.string.labelSquare)
        btnMinus.text  = getString(R.string.labelRoot)
        btnTimes.text  = getString(R.string.labelExponent)
        btnDivide.text = getString(R.string.labelInverse)
        
        btnCursor.text = getString(R.string.labelMode)
        btnTrig.text   = getString(R.string.labelLogs)
        btnSolv.text   = getString(R.string.labelPlot)
        btnUser.text   = getString(R.string.labelCustom)
        btnNext.text   = getString(R.string.labelPrev)
        btnChs.text    = getString(R.string.labelViewUp)
        btnEex.text    = getString(R.string.labelViewDown)
        btnDrop.text   = getString(R.string.labelRoll)
        btnBack.text   = getString(R.string.labelSwap)
        btnQuote.text  = getString(R.string.labelVisit)
        btnSto.text    = getString(R.string.labelRcl)
        btnEval.text   = getString(R.string.labelToNum)
        btnOn.text     = getString(R.string.labelOff)
        // Left keyboard
        btnA.text                  = getString(R.string.labelArray)
        btnB.text                  = getString(R.string.labelBinary)
        btnC.text                  = getString(R.string.labelComplx)
        btnD.text                  = getString(R.string.labelString)
        btnE.text                  = getString(R.string.labelList)
        btnF.text                  = getString(R.string.labelReal)
        btnG.text                  = getString(R.string.labelStack)
        btnH.text                  = getString(R.string.labelStore)
        btnI.text                  = getString(R.string.labelMemory)
        btnJ.text                  = getString(R.string.labelAlgbra)
        btnK.text                  = getString(R.string.labelStat)
        btnL.text                  = getString(R.string.labelPrint)
        btnM.text                  = getString(R.string.labelContrl)
        btnN.text                  = getString(R.string.labelBranch)
        btnO.text                  = getString(R.string.labelTest)
        btnP.text                  = getString(R.string.labelP)
        btnQ.text                  = getString(R.string.labelCatalog)
        btnR.text                  = getString(R.string.labelUnits)
        btnS.text                  = getString(R.string.labelLeq)
        btnT.text                  = getString(R.string.labelGeq)
        btnU.text                  = getString(R.string.labelTo)
        btnV.text                  = getString(R.string.labelSigma)
        btnW.text                  = getString(R.string.labelDegree)
        btnX.text                  = getString(R.string.labelMu)
        btnY.text                  = getString(R.string.labelLess)
        btnZ.text                  = getString(R.string.labelGreater)
        btnHash.text               = getString(R.string.labelDoubleQuote)
        btnCurlyBracket.text       = getString(R.string.labelRightCurlyBracket)
        btnSquareBracket.text      = getString(R.string.labelRightSquareBracket)
        btnParenthesis.text        = getString(R.string.labelRightParenthesis)
        btnSpace.text              = getString(R.string.labelNewline)
        btnDoubleAngleBracket.text = getString(R.string.labelRightDoubleAngleBracket)
        btnEqual.text              = getString(R.string.labelNotEqual)
        btnLC.text                 = getString(R.string.labelInterrogationPoint)
        btnAlpha.text              = getString(R.string.labelMenus)

        red = true
    }
    private fun updateButtonsToUnred()
    {
        btnOne.text    = getString(R.string.labelOne)
        btnTwo.text    = getString(R.string.labelTwo)
        btnThree.text  = getString(R.string.labelThree)
        btnFour.text   = getString(R.string.labelFour)
        btnFive.text   = getString(R.string.labelFive)
        btnSix.text    = getString(R.string.labelSix)
        btnSeven.text  = getString(R.string.labelSeven)
        btnEight.text  = getString(R.string.labelEight)
        btnNine.text   = getString(R.string.labelNine)
        btnZero.text   = getString(R.string.labelZero)
        btnPlus.text   = getString(R.string.labelPlus)
        btnMinus.text  = getString(R.string.labelMinus)
        btnTimes.text  = getString(R.string.labelTimes)
        btnDivide.text = getString(R.string.labelDivide)

        btnCursor.text = getString(R.string.labelCursor)
        btnTrig.text   = getString(R.string.labelTrig)
        btnSolv.text   = getString(R.string.labelSolv)
        btnUser.text   = getString(R.string.labelUser)
        btnNext.text   = getString(R.string.labelNext)
        btnChs.text    = getString(R.string.labelChs)
        btnEex.text    = getString(R.string.labelEex)
        btnDrop.text   = getString(R.string.labelDrop)
        btnBack.text   = getString(R.string.labelBack)
        btnQuote.text  = getString(R.string.labelQuote)
        btnSto.text    = getString(R.string.labelSto)
        btnEval.text   = getString(R.string.labelEval)
        btnOn.text     = getString(R.string.labelOn)
        
        // Left keyboard
        btnA.text                  = getString(R.string.labelA)
        btnB.text                  = getString(R.string.labelB)
        btnC.text                  = getString(R.string.labelC)
        btnD.text                  = getString(R.string.labelD)
        btnE.text                  = getString(R.string.labelE)
        btnF.text                  = getString(R.string.labelF)
        btnG.text                  = getString(R.string.labelG)
        btnH.text                  = getString(R.string.labelH)
        btnI.text                  = getString(R.string.labelI)
        btnJ.text                  = getString(R.string.labelJ)
        btnK.text                  = getString(R.string.labelK)
        btnL.text                  = getString(R.string.labelL)
        btnM.text                  = getString(R.string.labelM)
        btnN.text                  = getString(R.string.labelN)
        btnO.text                  = getString(R.string.labelO)
        btnP.text                  = getString(R.string.labelP)
        btnQ.text                  = getString(R.string.labelQ)
        btnR.text                  = getString(R.string.labelR)
        btnS.text                  = getString(R.string.labelS)
        btnT.text                  = getString(R.string.labelT)
        btnU.text                  = getString(R.string.labelU)
        btnV.text                  = getString(R.string.labelV)
        btnW.text                  = getString(R.string.labelW)
        btnX.text                  = getString(R.string.labelX)
        btnY.text                  = getString(R.string.labelY)
        btnZ.text                  = getString(R.string.labelZ)
        btnHash.text               = getString(R.string.labelHash)
        btnCurlyBracket.text       = getString(R.string.labelCurlyBracket)
        btnSquareBracket.text      = getString(R.string.labelSquareBracket)
        btnParenthesis.text        = getString(R.string.labelParenthesis)
        btnSpace.text              = getString(R.string.labelSpace)
        btnDoubleAngleBracket.text = getString(R.string.labelDoubleAngleBracket)
        btnEqual.text              = getString(R.string.labelEqual)
        btnLC.text                 = getString(R.string.labelLC)
        btnAlpha.text              = getString(R.string.labelAlpha)
        red = false
    }

    private fun onDrop(@Suppress("UNUSED_PARAMETER") view: View)
    {
        performEnter()
        if (red)
        { // Roll

        }
        else
        { // Drop
            if (operationStack.size >= 1)
                operationStack.removeAt(operationStack.size - 1)

        }
        updateStackView()
        updateButtonsToUnred()
    }

    private fun onDigit(view: View)
    {
        val tvResult: TextView = findViewById<TextView>(R.id.tvResult)

        if (tvResult.text == "0"){
            tvResult.text = ""
        }
        tvResult.append((view as Button).text)
        num= true
        dot = false
        have_expression = true
    }
    private fun onDot(@Suppress("UNUSED_PARAMETER") view: View)
    {
        val tvResult: TextView = findViewById<TextView>(R.id.tvResult)

        tvResult.append(".")
        num= true
        dot = true
        have_expression = true
    }
    private fun onComma(@Suppress("UNUSED_PARAMETER") view: View)
    {

    }
    private fun onChs(@Suppress("UNUSED_PARAMETER") view: View)
    {

    }
    private fun onEex(@Suppress("UNUSED_PARAMETER") view: View)
    {
        val tvResult: TextView = findViewById<TextView>(R.id.tvResult)

        tvResult.append("E")
        num= true
        //dot = true
        have_expression = true
    }
    private fun onEnter(@Suppress("UNUSED_PARAMETER") view: View)
    {
        performEnter()
        updateStackView()
    }
    private fun performEnter()
    {
        if (have_expression)
        {
            operationStack.add(tvResult.text.toString().toDouble())
            tvResult.text = ""
            have_expression = false
        }
    }
    private fun onPlus(@Suppress("UNUSED_PARAMETER") view: View)
    {
        performEnter()
        if (red)
        {
            if (operationStack.size >= 1)
                operationStack[operationStack.size - 1] = operationStack[operationStack.size - 1] * operationStack[operationStack.size - 1]
        }
        else
        {
            if (operationStack.size >= 2)
            {
                operationStack[operationStack.size - 2] =
                    operationStack[operationStack.size - 2] + operationStack[operationStack.size - 1]
                operationStack.removeAt(operationStack.size - 1)
            }
        }
        updateStackView()
        updateButtonsToUnred()
    }
    private fun onMinus(@Suppress("UNUSED_PARAMETER") view: View)
    {
        performEnter()
        if (red)
        {
            if (operationStack.size >= 1)
                operationStack[operationStack.size - 1] = Math.sqrt(operationStack[operationStack.size - 1])
        }
        else
        {
            if (operationStack.size >= 2)
            {
                operationStack[operationStack.size - 2] =
                    operationStack[operationStack.size - 2] - operationStack[operationStack.size - 1]
                operationStack.removeAt(operationStack.size - 1)
            }
        }
        updateStackView()
        updateButtonsToUnred()
    }
    private fun onTimes(@Suppress("UNUSED_PARAMETER") view: View)
    {
        performEnter()
        if (red)
        {
            if (operationStack.size >= 2)
            {
                operationStack[operationStack.size - 2] =
                    Math.pow(operationStack[operationStack.size - 2], operationStack[operationStack.size - 1])
                operationStack.removeAt(operationStack.size - 1)
            }
        }
        else
        {
            if (operationStack.size >= 2)
            {
                operationStack[operationStack.size - 2] =
                    operationStack[operationStack.size - 2] * operationStack[operationStack.size - 1]
                operationStack.removeAt(operationStack.size - 1)
            }
        }
        updateStackView()
        updateButtonsToUnred()
    }
    private fun onDivide(@Suppress("UNUSED_PARAMETER") view: View)
    {
        performEnter()
        if (red)
        { // Inverse
            if (operationStack.size >= 1)
                operationStack[operationStack.size - 1] = 1.0 / operationStack[operationStack.size - 1]
        }
        else
        { // Divide
            if (operationStack.size >= 2)
            {
                operationStack[operationStack.size - 2] =
                    operationStack[operationStack.size - 2] / operationStack[operationStack.size - 1]
                operationStack.removeAt(operationStack.size - 1)
            }
        }
        updateStackView()
        updateButtonsToUnred()
    }
    private fun update_menu()
    {
        btnInsert.text = menu_labels[current_menu.ordinal][0]
        btnDelete.text = menu_labels[current_menu.ordinal][1]
        btnUp.text     = menu_labels[current_menu.ordinal][2]
        btnDown.text   = menu_labels[current_menu.ordinal][3]
        btnLeft.text   = menu_labels[current_menu.ordinal][4]
        btnRight.text  = menu_labels[current_menu.ordinal][5]
    }
    // Menu Trigonometry
    private fun onTrig(@Suppress("UNUSED_PARAMETER") view: View)
    {
        if (red)
        { // Show the Logs menu
            current_menu = MENU.LOGS1
            update_menu()
            updateButtonsToUnred()
        }
        else
        { // Show the Trig menu
            current_menu = MENU.TRIG1
            update_menu()
        }
    }
    private fun onInsert(@Suppress("UNUSED_PARAMETER") view: View)
    {
        performEnter()
        if (current_menu == MENU.TRIG1)
        { // Sine
            if (operationStack.size >= 1)
                operationStack[operationStack.size - 1] = Math.sin(operationStack[operationStack.size - 1])
        }
        updateStackView()
        updateButtonsToUnred()
    }
    private fun onDelete(@Suppress("UNUSED_PARAMETER") view: View)
    {
        performEnter()
        if (current_menu == MENU.TRIG1)
        { // ArcSine
            if (operationStack.size >= 1)
                operationStack[operationStack.size - 1] = Math.asin(operationStack[operationStack.size - 1])
        }
        updateStackView()
        updateButtonsToUnred()
    }
    private fun onUp(@Suppress("UNUSED_PARAMETER") view: View)
    {
        performEnter()
        if (current_menu == MENU.TRIG1)
        { // Cosine
            if (operationStack.size >= 1)
                operationStack[operationStack.size - 1] = Math.cos(operationStack[operationStack.size - 1])
        }
        updateStackView()
        updateButtonsToUnred()
    }
    private fun onDown(@Suppress("UNUSED_PARAMETER") view: View)
    {
        performEnter()
        if (current_menu == MENU.TRIG1)
        { // ArcCosine
            if (operationStack.size >= 1)
                operationStack[operationStack.size - 1] = Math.acos(operationStack[operationStack.size - 1])
        }
        updateStackView()
        updateButtonsToUnred()
    }
    private fun onLeft(@Suppress("UNUSED_PARAMETER") view: View)
    {
        performEnter()
        if (current_menu == MENU.TRIG1)
        { // Tangent
            if (operationStack.size >= 1)
                operationStack[operationStack.size - 1] = Math.tan(operationStack[operationStack.size - 1])
        }
        updateStackView()
        updateButtonsToUnred()
    }
    private fun onRight(@Suppress("UNUSED_PARAMETER") view: View)
    {
        performEnter()
        if (current_menu == MENU.TRIG1)
        { // AecTangent
            if (operationStack.size >= 1)
                operationStack[operationStack.size - 1] = Math.atan(operationStack[operationStack.size - 1])
        }
        updateStackView()
        updateButtonsToUnred()
    }
}